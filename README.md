##View page locally
Start http server and load page with the following command:
  $ http-server -o

The project is also hosted at:
[http://wesgabbard.com/atlassian/index.html](http://wesgabbard.com/atlassian/index.html)


##Build
To get started and install dependencies: 
  $ npm install

To start build:
  $ grunt build

To watch:
  $ grunt